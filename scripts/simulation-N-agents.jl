##
using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Agents
using Agents: AbstractSpace
using BSON
using CSVFiles
using Chain
using DataFrames
using Distances
using Distributions
using IntervalArithmetic
using LinearAlgebra
using MultivariateStats
using ODEDNNA
using StaticArrays
using StatsBase
using Queryverse
using UnicodeFun

using RCall
@rlibrary ggplot2
@rlibrary patchwork
@rlibrary ggthemes
@rlibrary ggpointdensity
@rlibrary ggridges
@rlibrary latex2exp
@rlibrary scales

include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

##
simulator_N_agents = SimulationSetup(
                                agent_step! = entropic_dynamics!,
                                suffix      = "bson",
                                path        = datadir("sims","N_greater_than_2")
                            )


##
function scenario_trustful_N_agents(; K, N, C0, V0, betam0, P, props...)
    w0 = sqrt(1 + C0)
    m0 = -sqrt(1 + V0)*betam0
    agents = [odagent(
                    i,
                    normalize!(randn(K)).*w0,
                    rand_pd_matrix(K;v=C0),
                    [i==j ? 0.0 : 1.0 for j in 1:N].*m0,
                    [i==j ? 0.0 : 1.0 for j in 1:N].*V0
                ) for i in 1:N]
    issues = random_issues(;K, P)
    
    return odsociety(; agents, issues, props...)
end

let force=false, parameters=(K=5,N=30,C0=1.0,V0=1.0,betam0=[0.1,1.0],P=1)
    d = complexity(parameters.K,parameters.N)
    
    m_sqrt1pV(a::ODAgent) = a.m./sqrt.(1.0.+a.V)

    trust_simulator = simulator_N_agents[
                            model_init=scenario_trustful_N_agents,
                            steps=d,
                            adata=[:w,:m,m_sqrt1pV],
                            when=periodic_report(10),
                            mdata=[overlap,distrust],
                            when_model=periodic_report(10),
                            prefix="trustful",                  
                        ]
    datadict,fname = trust_simulator(; force, parameters...)
    
    data = datadict[:adata]
    pca_data = @chain data begin
        groupby([:betam0,:step])
        pairs
        map(_) do (k,gdf)
            ws = mapreduce(normalize,hcat,gdf.w)
            ms = mapreduce(normalize,hcat,gdf.m_sqrt1pV)
            (betam0=k.betam0,step=k.step,W=ws,M=ms)
        end
        DataFrame
        groupby(:betam0)
        pairs
        map(_) do (k,gdf)
            wt = cat(gdf.W...; dims=3)
            mt = cat(gdf.M...; dims=3)
            pca1_w = add_dynamics_to_pca_tensor(multilinear_pca(wt))
            pca1_m = add_dynamics_to_pca_tensor(multilinear_pca(mt))
            neighbor_fraction_w = compute_neighbor_fraction_from_tensor(wt,overlap; cutoff=0.95)
            neighbor_fraction_m = compute_neighbor_fraction_from_tensor(mt,distrust; cutoff=0.95)
            dfw = tidy_array(pca1_w[1,:,:]; value_name=:pca1_w, col_names=[:id,:dim_steps])
            dfm = tidy_array(pca1_m[1,:,:]; value_name=:pca1_m, col_names=[:id,:dim_steps])
            df = leftjoin(dfw,dfm; on=[:id,:dim_steps])
            df[!,:betam0] .= k.betam0
            df[!,:w_close] = neighbor_fraction_w[:]
            df[!,:m_close] = neighbor_fraction_m[:]
            select!(df,:dim_steps=>ByRow(x->unique(gdf.step)[x])=>:step,Not(:dim_steps))
        end
        vcat(_...)
        stack([:pca1_w,:pca1_m]; value_name=:pca)
        stack([:w_close,:m_close]; value_name=:neighbors_fraction,variable_name=:neighbors_space)
        transform!(:step=>ByRow(x->log10(1+x))=>:log10_t,
                   :betam0=>ByRow(x->"beta[m0] == $x")=>:ic,
                   :variable=>ByRow(x->(v=split(string(x),"_")[2];"DBPCA[1]($v(t))"))=>:variable)
        
    end
   
    trustful_plot = ggplot(pca_data,aes(x=:log10_t,color=:neighbors_fraction)) + 
        geom_point(aes(y=:pca),size=1,shape=21,alpha=1/5) + 
        scale_x_log10() + 
        scale_color_viridis_c(option="magma",
                              var"begin"=0.1, var"end"=0.9,
                              guide=guide_colorbar(title="Fraction of\nAgents nearby",
                                                   limits=[0.0,1.0],
                                                   breaks=[0.0,0.5,1.0],
                                                   barwidth=0.5, barheight=10)) +
        facet_grid(R"variable ~ ic", labeller=label_parsed) + 
        labs(x=TeX(raw"$log_{10} t$"),y="") +
        theme(var"strip.background" = element_blank()) + 
        theme(var"strip.text.y" = element_text(angle=0))

    ggsave(savename("trustful-dpca",ntuple2dict(parameters),"pdf"),
            trustful_plot,
            path=plotsdir("simulations","N_greater_than_2"),
            height=6,width=10)

    trustful_plot
end

##
function scenario_consensus_N_agents(; K, N, C0, V0, betaw0, P, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0).*1.0/N
    
    agents = [odagent(
                    i,
                    normalize!(ones(K) + 0.1randn(K)).*w0,
                    rand_pd_matrix(K;v=C0),
                    [i==j ? 0.0 : randn() for j in 1:N]*m0,
                    [i==j ? 0.0 : 1.0 for j in 1:N].*V0
                ) for i in 1:N]

    issues = random_issues(;K, P)
    
    return odsociety(; agents, issues, props...)
end


let force=false, parameters=(K=5,N=30,C0=1.0,V0=1.0,betaw0=[1.0/30,1.0],P=1)
    d = complexity(parameters.K,parameters.N)
    
    m_sqrt1pV(a::ODAgent) = a.m./sqrt.(1.0.+a.V)

    agree_simulator = simulator_N_agents[
                            model_init=scenario_consensus_N_agents,
                            steps=d,
                            adata=[:w,:m,m_sqrt1pV],
                            when=periodic_report(10),
                            mdata=[overlap,distrust],
                            when_model=periodic_report(10),
                            prefix="consensus",                  
                        ]
    datadict,fname = agree_simulator(; force, parameters...)
    
    data = datadict[:adata]
    pca_data = @chain data begin
        groupby([:betaw0,:step])
        pairs
        map(_) do (k,gdf)
            ws = mapreduce(normalize,hcat,gdf.w)
            ms = mapreduce(normalize,hcat,gdf.m_sqrt1pV)
            (betaw0=k.betaw0,step=k.step,W=ws,M=ms)
        end
        DataFrame
        groupby(:betaw0)
        pairs
        map(_) do (k,gdf)
            wt = cat(gdf.W...; dims=3)
            mt = cat(gdf.M...; dims=3)
            pca1_w = add_dynamics_to_pca_tensor(multilinear_pca(wt))
            pca1_m = add_dynamics_to_pca_tensor(multilinear_pca(mt))
            neighbor_fraction_w = compute_neighbor_fraction_from_tensor(wt,overlap; cutoff=0.95)
            neighbor_fraction_m = compute_neighbor_fraction_from_tensor(mt,distrust; cutoff=0.95)
            dfw = tidy_array(pca1_w[1,:,:]; value_name=:pca1_w, col_names=[:id,:dim_steps])
            dfm = tidy_array(pca1_m[1,:,:]; value_name=:pca1_m, col_names=[:id,:dim_steps])
            df = leftjoin(dfw,dfm; on=[:id,:dim_steps])
            df[!,:betaw0] .= k.betaw0
            df[!,:w_close] = neighbor_fraction_w[:]
            df[!,:m_close] = neighbor_fraction_m[:]
            select!(df,:dim_steps=>ByRow(x->unique(gdf.step)[x])=>:step,Not(:dim_steps))
        end
        vcat(_...)
        stack([:pca1_w,:pca1_m]; value_name=:pca)
        stack([:w_close,:m_close]; value_name=:neighbors_fraction,variable_name=:neighbors_space)
        transform!(:step=>ByRow(x->log10(1+x))=>:log10_t,
                   :betaw0=>ByRow(x->"beta[w0] == $x")=>:ic,
                   :variable=>ByRow(x->(v=split(string(x),"_")[2];"DBPCA[1]($v(t))"))=>:variable)
        
    end
   
    consensus_plot = ggplot(pca_data,aes(x=:log10_t,color=:neighbors_fraction)) + 
        geom_point(aes(y=:pca),size=1,shape=21,alpha=1/5) + 
        scale_x_log10() + 
        scale_color_viridis_c(option="magma",
                              var"begin"=0.1, var"end"=0.9,
                              guide=guide_colorbar(title="Fraction of\nAgents nearby",
                                                   limits=[0.0,1.0],
                                                   breaks=[0.0,0.5,1.0],
                                                   barwidth=0.5, barheight=10)) +
        facet_grid(R"variable ~ ic", labeller=label_parsed) + 
        labs(x=TeX(raw"$log_{10} t$"),y="") +
        theme(var"strip.background" = element_blank()) + 
        theme(var"strip.text.y" = element_text(angle=0))

    ggsave(savename("consensus-dpca",ntuple2dict(parameters),"pdf"),
    consensus_plot,
            path=plotsdir("simulations","N_greater_than_2"),
            height=6,width=10)

    consensus_plot
end