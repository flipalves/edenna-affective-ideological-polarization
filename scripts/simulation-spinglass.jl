##
using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

NCORES = length(Sys.cpu_info())
using Distributed
nprocs()<NCORES && addprocs(NCORES)

@everywhere using DrWatson
@everywhere @quickactivate "ednna-affective-ideological-polarization"

@everywhere begin
    using Agents
    using Agents: AbstractSpace
    using BSON
    # using Chain
    using DataFrames
    using Distances
    using Distributions
    using LinearAlgebra
    using MultivariateStats
    using ODEDNNA
    using Random
    # using StaticArrays
    using StatsBase
end

using CSVFiles
# using IntervalArithmetic
# using Queryverse
# using UnicodeFun

# using RCall
# @rlibrary ggplot2
# @rlibrary patchwork
# @rlibrary ggthemes
# @rlibrary ggpointdensity
# @rlibrary ggridges
# @rlibrary latex2exp
# @rlibrary scales

@everywhere begin
    include(srcdir("util.jl"))
    include(srcdir("data-collection.jl"))
end 

##
@everywhere function scenario_spin_glass(; K, N, C0, V0, P, betaw0, betam0, seed, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0)*betam0

    agents = ODAgentIHC{K,N}|>
             rand.|>
             normalize!.|>
             x->scale_agent!(x; w=w0, m=m0, C=C0, V=V0)

    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)

    return odsociety(; agents, issues, rng, props...)
end

##
function run_sim(;sweeps=1,replicates=1, force=false)
    parameters=(K=20,N=20,C0=10.0,V0=10.0,betaw0=1.0,betam0=1.0)
    #K = parameters.K
    #P = [1:K...,2K,5K,10K,20K,50K,100K]
    # following my thesis 
    P = [1,2,4,8,16,20,24,48,100,400,8_000]#,160_000]
    seed = [rand(UInt8) for _ in 1:replicates]
    parameters = (;parameters...,P,seed)
    d = complexity(parameters.K,parameters.N)
    total_steps = sweeps*d
    # report_instants(ts) = (m,s)->s in ts
    # instants_to_report = (Int∘round).(StatsBase.quantile(0:total_steps,[0.0,0.25,0.5,0.75,1.0]))

    spinglass_simulator = SimulationSetup(
                            model_init=scenario_spin_glass,
                            agent_step! = entropic_dynamics!,
                            # adata=[:w,:m,m_sqrt1pV],
                            # when=periodic_report(10),
                            mdata=[mean_balance],
                            when_model=periodic_report(parameters.N),#report_instants(instants_to_report),#periodic_report(10),
                            steps=total_steps,
                            parallel=true,
                            prefix="spinglass-thesis",
                            suffix="bson",
                            path=datadir("sims","N_greater_than_2")
                        )

    datadict, fname = spinglass_simulator(; force, parameters...)
    
    data = datadict[:mdata]
    summary_data = data |>
        df->groupby(df,[:step,:P])|>
        df->combine(df,:mean_balance=>mean=>:avg_mean_balance)|>
        df->transform!(df,:P=>ByRow(x->"P == $x")=>:number_of_issues,:step=>ByRow(x->log10(1+x))=>:log10_t)|>
        DataFrame

    return summary_data, parameters
end
    
# function plot_sim(data, parameters)
#     spinglass_plot = ggplot(data, aes(x=:log10_t,y=:avg_mean_balance,color=:number_of_issues)) +
#         geom_point(size=1,shape=21,alpha=7/10) + geom_line()+
#         scale_color_viridis_d()

#     ggsave(savename("spinglass-balance",ntuple2dict(parameters),"pdf"),
#             spinglass_plot,
#             path=plotsdir("simulations","N_greater_than_2"),
#             height=6,width=10)

#     return spinglass_plot
# end
##
