##
using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Agents
using Agents: AbstractSpace
using BSON
using CSVFiles
# using Chain
using DataFrames
using Distances
using Distributions
# using Gadfly
using LinearAlgebra
using MultivariateStats
using ODEDNNA
# using PlotlyJS
# using PGFPlotsX
using StaticArrays
using Queryverse
using UnicodeFun

using RCall
@rlibrary ggplot2
@rlibrary patchwork
@rlibrary ggthemes
@rlibrary ggpointdensity
@rlibrary ggridges
@rlibrary latex2exp
@rlibrary scales

include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

##
function scenario_2_agents(; K, rho0, C0, m10, m20, V0, P, props...)
    c = C0/sqrt(K)
    w1, w2 = rand_pair_from_overlap(K,rho0)
    C1, C2 = c.*idmat(K),c.*idmat(K)
    m1, m2 = [0.0, m10], [m20, 0.0]
    V1, V2 = [0.0, V0], [V0, 0.0]
    
    agents = [odagent(1,w1,C1,m1,V1), odagent(2,w2,C2,m2,V2)] 
    issues = random_issues(; K, P)

    return odsociety(; agents, issues, props...)
end

##
simulator_2_agents = SimulationSetup(
                model_init = scenario_2_agents,
                agent_step! = entropic_dynamics!,
                when = periodic_report,
                when_model = periodic_report,
                suffix = "bson", 
                path = datadir("sims","N=2")
            )

##
let force=false, parameters=(K=5,rho0=0.5,C0=1.0,m10=0.25,m20=-0.25,V0=[0.001,0.01,0.1,1.0],P=5^3)
    d = ODEDNNA.complexity(parameters.K,2)
    simulator_2_agents_example = simulator_2_agents[
                                        prefix="example",
                                        steps=d^4,
                                        adata = [distrust,agent_norms()...],
                                        mdata = [overlap]
                                    ]
    data,fname = simulator_2_agents_example(;force, parameters...)
    CSVFiles.save(
                    datadir("sims","N=2",
                    savename("example-adata",parameters,"csv")),
                    data[:adata]
                )
    CSVFiles.save(
        datadir("sims","N=2",
        savename("example-mdata",parameters,"csv")),
        data[:mdata]
    )

    adata = data[:adata] |>
        @mutate(ln_tau=log(d,1+_.step),
                emitter_receiver=_.id==1 ? "epsilon[2|1]" : "epsilon[1|2]",
                agent="Agent $(_.id)",
                initial_cond="V0=$(_.V0)") |>
        @select(-:step, -:V0, -:id) |>
        DataFrame |>
        df->stack(df,[:norm_w,:norm_m]; 
                     variable_name=:weights_variable, 
                     value_name=:weights_norm) |>
        df->stack(df,[:log10_norm_C,:log10_norm_V];
                     variable_name=:uncertainty_variable,
                     value_name=:uncertainty_norm)

    mdata = data[:mdata] |>
        @mutate(ln_tau=log(d,1+_.step),initial_cond="V[0] == $(_.V0)")|>
        @select(-:V0, -:step)|>
        DataFrame

    prho = ggplot(mdata, aes(x=:ln_tau,y=:overlap)) + 
            geom_line(size=1,alpha=0.8) + 
            facet_wrap(R"~initial_cond", nrow=1, labeller="label_parsed") +
            labs(x="", y=TeX(raw"Overlap $(\rho)$")) + 
            theme(var"strip.background" = element_blank()) + 
            theme(var"axis.title.x"=element_blank(),
                  var"axis.text.x"=element_blank(),
                  var"axis.ticks.x"=element_blank())

    peps = ggplot(adata,aes(x=:ln_tau, y=:distrust, color=:emitter_receiver)) + 
            geom_line(size=1,alpha=0.8) + 
            facet_wrap(R"~initial_cond", nrow=1) +
            labs(x="", y=TeX(raw"Distrust $(\epsilon)$")) +
            scale_color_tableau(labels=[TeX(raw"$\epsilon_{2|1}$"),TeX(raw"$\epsilon_{1|2}$")]) + 
            theme(var"strip.background" = element_blank(),var"strip.text.x"=element_blank()) + 
            theme(var"axis.title.x"=element_blank(),
                  var"axis.text.x"=element_blank(),
                  var"axis.ticks.x"=element_blank()) + 
            theme(var"legend.title"=element_blank())

    punc = ggplot(adata, aes(
                                x=:ln_tau, 
                                y=:uncertainty_norm,
                                color=:agent,
                                linetype=:uncertainty_variable
                            )) +
            geom_line(size=1,alpha=0.8) + 
            facet_wrap(R"~initial_cond", nrow=1) +
            labs(x=TeX(raw"$\mathrm{log}_{N(K+N-1)} t$"),y=TeX(raw"Uncertainties ($|C|,|V|$)")) + 
            scale_linetype(labels=[TeX(raw"$log_{10}|C|$"),TeX(raw"$log_{10}|V|$")]) +  
            theme(var"strip.background" = element_blank(),var"strip.text.x"=element_blank()) + 
            theme(var"legend.title"=element_blank())
    
    example_plot = (prho / peps / punc)
    
    ggsave(savename("example",ntuple2dict(parameters),"pdf"),
                    example_plot,
                    path=plotsdir("simulations","N=2"),
                    height=9,width=16)

    example_plot
end

##
let force=false, parameters=(K=5,rho0=0.5,C0=1.0,m10=0.25,m20=-0.25,V0=[0.001,0.01,0.1,1.0],P=5^3)
    d = ODEDNNA.complexity(parameters.K,2)
    simulator_2_agents_statistics = simulator_2_agents[
                                prefix="statistics",
                                adata=[distrust],mdata=[overlap],
                                when=logarithmic_report,when_model=logarithmic_report, 
                                steps=d^4,replicates=1000
                            ]
    data,fname = simulator_2_agents_statistics(;force, parameters...)

    mdata, adata = data[:mdata],data[:adata]
    jdata = leftjoin(adata,mdata; on=[:step,:replicate,:V0])
    transform!(jdata,:step=>ByRow(x->"t == (N*(K+N-1))^$(log(d,1+x))")=>:time,
                     :V0=>ByRow(x->"V[0] == $x")=>:ic)
    
    # taus = unique(jdata.ln_tau)
    # v0s = unique(jdata.V0)
    # @rput taus
    # @rput v0s
    # R"library('latex2exp')"
    # R"library('scales')"

    # ln_tau_labels = reval("c($(join(("`$x`='TeX(\$log_{N(K+N-1)} t = $x\$)'" for x in 0:4),", ")))")
    # V0_labels = reval("c($(join(("`$x`='TeX(\$V_0 = $x\$)'" for x in (0.001,0.01,0.1,1.0)),", ")))")

    statistics_plot = ggplot(jdata,aes(x=:overlap, y=:distrust)) + 
        geom_pointdensity(size=3,shape=21,adjust=1.75) + 
        scale_color_viridis_c(option="magma",
                              var"begin"=0.1, var"end"=0.9,
                              guide=guide_colorbar(title="Count",
                                                   barwidth=0.5, barheight=10)) + 
        facet_grid(R"time ~ ic", labeller=label_parsed) + 
        labs(x=TeX(raw"Overlap $(\rho)$"),y=TeX(raw"Distrust $(\epsilon)$")) +
        theme(var"strip.background" = element_blank()) + 
        theme(var"strip.text.y" = element_text(angle = 0))

    ggsave(savename("statistics",ntuple2dict(parameters),"pdf"),
                    statistics_plot,
                    path=plotsdir("simulations","N=2"),
                    height=9,width=16)

    statistics_plot
end