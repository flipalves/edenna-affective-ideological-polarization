##
using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Distributed
NCORES = length(Sys.cpu_info())
nprocs()<NCORES && addprocs(NCORES)

@everywhere using DrWatson 
@everywhere begin    
    @quickactivate "ednna-affective-ideological-polarization"
end

using Agents
using Agents: AbstractSpace
using BSON
using DataFrames
using Distances
using Distributions
using LinearAlgebra
using MultivariateStats
using ODEDNNA
using Random
using StatsBase

@everywhere include(srcdir("spinglass-base.jl"))

@everywhere begin 
    include(srcdir("util.jl"))
    include(srcdir("data-collection.jl"))
end

include(srcdir("spinglass-base.jl"))
include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

using CSVFiles

##
"Scenario described in my phd thesis"
function run_thesis_scenario(; sweeps=1,replicates=1, force=false)
    parameters = (K=20,N=20,C0=10.0,V0=10.0,betaw0=1.0,betam0=1.0) 
    P = [1,2,4,8,16,20,24,48,100,400,8_000,160_000]
    seed = [rand(UInt8) for _ in 1:replicates]
    parameters = (; parameters..., P, seed)
    d = complexity(parameters.K, parameters.N)
    total_steps = sweeps*d

    spinglass_thesis_simulator = SimulationSetup(
                            model_init = spinglass_init,
                            agent_step! = entropic_dynamics!,
                            mdata=[mean_balance],
                            when_model=true,
                            steps=total_steps,
                            parallel=true,
                            prefix="spinglass-thesis",
                            suffix="bson",
                            path=datadir("sims","N_greater_than_2")
                        )

    datadict, fname = spinglass_thesis_simulator(; force, parameters...)
    
    data = datadict[:mdata]
    summary_data = data |>
        df->groupby(df,[:step,:P])|>
        df->combine(df,:mean_balance=>mean=>:avg_mean_balance)|>
        df->transform!(df,:P=>ByRow(x->"P == $x")=>:number_of_issues,:step=>ByRow(x->log10(1+x))=>:log10_t)|>
        DataFrame

    
    # CSVFiles.save(replace(fname,"bson"=>"csv"), summary_data)   

    return (; summary_data, parameters, fname)
end
    
# function plot_sim(data, parameters)
#     spinglass_plot = ggplot(data, aes(x=:log10_t,y=:avg_mean_balance,color=:number_of_issues)) +
#         geom_point(size=1,shape=21,alpha=7/10) + geom_line()+
#         scale_color_viridis_d()

#     ggsave(savename("spinglass-balance",ntuple2dict(parameters),"pdf"),
#             spinglass_plot,
#             path=plotsdir("simulations","N_greater_than_2"),
#             height=6,width=10)

#     return spinglass_plot
# end
##
