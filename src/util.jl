idmat(n; eltype=Float64) = Matrix{eltype}(I,n,n)
normalize(v::AbstractVector) = v./norm(v)

random_issues(; K, P=1) = [normalize!(randn(K)) for _ in 1:P]

function rand_pair_from_overlap(K, ρ) 
    v,u = selectdim.([qr(randn(K,K)).Q],2,[1,2])
    return (v, ρ.*v + sin(acos(ρ)).*u)
end

function rand_pd_matrix(K; v=1.0)
    m = rand(K,K)
    m *= m'./2
    m += K*I
    m .*= v/norm(m)
    return m
end

function base_distrust_vec(i,N;val=0.0)
    m = ones(N)
    m[i] = val
    return m
end

