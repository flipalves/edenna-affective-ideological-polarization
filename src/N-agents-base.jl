using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Agents
using Agents: AbstractSpace
using BSON
using CSVFiles
using DataFrames
using Distances
using Distributions
using LightGraphs
using LinearAlgebra
using MultivariateStats
using ODEDNNA
using Random
using Queryverse
using StatsBase

include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

##

_complete_args(a,args) = isempty(args) ? a : (va=[a...]; tuple((x isa Colon ? popfirst!(va) : x for x in args)...))
function Base.getindex(f::F,args...;kwargs...) where {F<:Function}
    partial(a...;k...) = f(_complete_args(a,args)...;merge(kwargs,k)...)
    return partial
end

function shift_society!(agents; w::Union{Float64,<:AbstractVector}=0.0, m::Union{Float64,<:AbstractVector}=0.0)
    K, N = typeof(agents[1]).parameters
    for a in agents
        shift_agent!(a; w=w.*ones(K), m=m.*[i==a.id ? 0.0 : 1.0 for i in 1:N])
    end
    return agents
end

# shift_society!(; w=0.0, m=0.0) = agents->shift_society!(agents; w, m)

function scale_society!(agents; w::Float64=1.0, C::Float64=1.0, m::Float64=1.0, V::Float64=1.0)
    for a in agents
        scale_agent!(a; w, C, m, V)
    end
    return agents
end

# scale_society!(; w=1.0, C=1.0, m=1.0, V=1.0) = s->scale_society!(s; w, C, m, V)

function _initial_consensus(agents,Δw=1.0,αw=0.1; w0, C0, m0, V0)
    agents|>
        scale_society![w=αw]|>
        shift_society![w=Δw].|>
        normalize!|>
        scale_society![w=w0, C=C0, m=m0, V=V0]        
    return agents
end

function _initial_trustful(agents,Δm=-1.0,αm=0.05; w0, C0, m0, V0)
    agents|>
        scale_society![m=αm]|>
        shift_society![m=Δm].|>
        normalize!|>
        scale_society![w=w0, C=C0, m=m0, V=V0]
    return agents
end

_initial_conditions(scenario::Val{:consensus}; w0,C0,m0,V0) = _initial_consensus[w0=w0,C0=C0,m0=m0,V0=V0]
_initial_conditions(scenario::Val{:trustful}; w0,C0,m0,V0) = _initial_trustful[w0=w0,C0=C0,m0=m0,V0=V0]
_initial_conditions(scenario::Symbol; kw...) = _initial_conditions(Val(scenario); kw...)
    
function init_N_agents(; agent_type, K, N, C0, V0, betaw0, betam0, P, scenario, seed, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0)*betam0 
    
    agents = agent_type{K,N}|>
                rand|>
                _initial_conditions(scenario; w0, C0, m0, V0)
    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)
    scheduler = Schedulers.partially(1/N)
    topology = complete_graph(N)
    
    return odsociety(; agents, issues, rng, scheduler, topology, props...)
end

# init_N_agents(; agent_type=ODAgent, kw...) = init_N_agents(agent_type; kw...)

# function scenario_trustful_N_agents(A::Type{<:AbstractEDNNAgent}; K, N, C0, V0, P, betam0, betaw0, seed, props...)
#     w0 = sqrt(1 + C0)*betaw0
#     m0 = sqrt(1 + V0)*betam0
#     agents = [
#         A{K,N}(
#             i,i,
#             normalize!(randn(K)).*w0,
#             ODEDNNA._C_identity(A{K,N}), #rand_pd_matrix(K;v=C0),
#             [i==j ? 0.0 : -1.0 for j in 1:N].*m0,
#             [i==j ? 0.0 : 1.0 for j in 1:N].*V0
#         ) for i in 1:N]
#     issues = random_issues(;K, P)
#     rng = Random.MersenneTwister(seed)
#     topology = complete_graph(N)
#     scheduler = Schedulers.partially(1/N)
    
#     return odsociety(; agents, issues, rng, topology, scheduler, props...)
# end

# function scenario_consensus_N_agents(; K, N, C0, V0, betaw0, P, props...)
#     w0 = sqrt(1 + C0)*betaw0
#     m0 = sqrt(1 + V0).*1.0/N
    
#     agents = [odagent(
#                     i,
#                     normalize!(ones(K) + 0.1randn(K)).*w0,
#                     rand_pd_matrix(K;v=C0),
#                     [i==j ? 0.0 : randn() for j in 1:N]*m0,
#                     [i==j ? 0.0 : 1.0 for j in 1:N].*V0
#                 ) for i in 1:N]

#     issues = random_issues(;K, P)
    
#     return odsociety(; agents, issues, props...)
# end