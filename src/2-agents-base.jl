using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Agents
using Agents: AbstractSpace
using BSON
using DataFrames
using Distances
using Distributions
using LightGraphs
using LinearAlgebra
using MultivariateStats
using ODEDNNA
using Random
using Queryverse
using StatsBase

include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

##
function scenario_2_agents_init(A::Type{<:AbstractEDNNAgent}; K=5, rho0=0.0, C0=1.0, m10=1.0, m20=-1.0, V0=1.0, P=1, seed=1234, props...)
    c = C0/sqrt(K)
    w1, w2 = rand_pair_from_overlap(K,rho0)
    C1, C2 = c.*ODEDNNA._C_identity(A{K,2}), c.*ODEDNNA._C_identity(A{K,2})
    m1, m2 = [0.0, m10], [m20, 0.0]
    V1, V2 = [0.0, V0], [V0, 0.0]
    
    agents = [A{K,2}(1,1,w1,C1,m1,V1), A{K,2}(2,2,w2,C2,m2,V2)] 
    issues = random_issues(; K, P)
    rng = Random.MersenneTwister(seed)
    scheduler = Schedulers.randomly
    topology = complete_graph(2)

    return odsociety(; agents, issues, rng, scheduler, topology, props...)
end

function scenario_2_agents_init(; agent_type=ODAgent, params...)
    scenario_2_agents_init(agent_type; params...)
end