import ODEDNNA: Φ, G, γ, complexity
using Agents: AbstractSpace

# reporter schedule
periodic_report(p::Int) = (model,step)-> step % p == 0
periodic_report(model,step) = periodic_report(complexity(model))(model,step)

logarithmic_report(k::Float64) = (model,step) -> log(k,1+step) in 0:10
logarithmic_report(model,step) = log(complexity(model),1+step) in 0:10

const Agent{K,N} = Union{ODAgent{K,N},ODAgentIHC{K,N}} where {K,N}
const Society{K,N} = ABM{<:AbstractSpace,<:Agent{K,N}} where {K,N}

# reporter functions
function agent_norms()
    norm_w(a::Agent) = norm(a.w)
    log10_norm_C(a::Agent) = log10(norm(a.C))
    norm_m(a::Agent) = norm(a.m)
    log10_norm_V(a::Agent) = log10(norm(a.V))
    return [norm_w,log10_norm_C,norm_m,log10_norm_V]
end

function cosinedist(s::Society,p::Symbol)
    return pairwise(CosineDist(),(getfield(s[i],p) for i in by_id(s)))
end

cossine(s::Society, p::Symbol) = 1.0 .- cosinedist(s,p)

overlap(s::Society) = cossine(s,:w)

function overlap(s::Society{K,2}) where {K}
    return dot(normalize(s[1].w),normalize(s[2].w))
end

m_overlap(s::Society) = cossine(s,:m)

function distrust(a::A) where {A<:AbstractEDNNAgent}
    e = @. Φ(a.m/γ(a.V))
    e[a.id] = 0.0
    return e
end

function distrust(a::Agent{K,2}) where {K} 
    e = a.id==1 ? 2 : 1
    return Φ(a.m[e]/γ(a.V[e]))
end

function distrust(s::Society)# where {A<:AbstractEDNNAgent}
    eps = (distrust(s[i]) for i in by_id(s))
    return hcat(eps...)
end

# Utilities to deal with mdata returning arrays 
function tidy_array(A::Array{<:Any,N}; value_name=:value, col_names=[Symbol("dim_$n") for n in 1:N]) where N
    idx = vcat((hcat(c.I...) for c in CartesianIndices(A)[:])...)
    df = DataFrame(idx,col_names)
    df[!,value_name] = A[:]
    return df
end

function pca(X::Array{Float64,2})
    C = cov(X; dims=2)
    E = eigen(C)
    P = E.vectors[:,sortperm(E.values;rev=true)]
    # should be (X .- mean(X; dims=2)) 
    # but for some reason, centralizing introduces a bias in my case
    pca_X = P'*X 
    return  pca_X
end

function multilinear_pca(T::Array{Float64,3}; kw...)
    mpca = cat((pca(T[:,:,t]) for t in 1:size(T,3))...; dims=3)
    return mpca
end

function add_dynamics_to_pca_tensor(X::Array{Float64,3})
    D = cat((pairwise(CosineDist(),X[:,:,t]) for t in 1:size(X,3))...;dims=3)
    C = 1.0 .- D
    M = mean(C; dims=[1,2])
    V = std(C; dims=[1,2])
    return V.*X .+ M
end

function compute_neighbor_fraction_from_tensor(X::Array{Float64,3}, Dt::Function; cutoff=0.95)
    D = cat((Dt(X[:,:,t]) for t in 1:size(X,3))...; dims=3)
    N = sum(abs.((D.-mean(D;dims=[1,2]))./std(D;dims=[1,2])).<cutoff; dims=1) ./ size(X,2)
    return N
end

function overlap(w::Matrix{Float64})
    return pairwise(CosineDist(),w)
end

function distrust(m::Matrix{Float64})
    return hcat((Φ.(mi) for mi in eachcol(m))...)
end

function transitivity(t::M) where {M<:AbstractMatrix}
    N = size(t,2)
    triples = (c.I for c in CartesianIndices((N,N,N)) if allunique(c.I))
    transitive_triples = (t[j,i]*t[j,k]*t[k,i] for (i,j,k) in triples)
    return transitive_triples
end

mean_distrust_transitivity(s::Society) = mean(transitivity(2.0.*distrust(s).-1.0))

mean_agreement_transitivity(s::Society) = mean(transitivity(overlap(s)))

function mean_transitivity_consistency(s::Society)
    dt = transitivity(2.0.*distrust(s).-1.0)
    da = transitivity(overlap(s))
    opdis_consistency = (a*d for a in da for d in dt)
    return mean(opdis_consistency)
end