using DrWatson
@quickactivate "ednna-affective-ideological-polarization"

using Agents
using Agents: AbstractSpace
using BSON
using DataFrames
using Distances
using Distributions
using LightGraphs
using LinearAlgebra
using MultivariateStats
using ODEDNNA
using Random
using StatsBase

include(srcdir("util.jl"))
include(srcdir("data-collection.jl"))

function spinglass_init(A::Type{<:AbstractEDNNAgent}; K=20, N=20, C0=10.0, V0=10.0, P=1, betaw0=1.0, betam0=1.0, seed=1234, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0)*betam0

    agents = A{K,N}|>
             rand.|>
             normalize!.|>
             x->scale_agent!(x; w=w0, m=m0, C=C0, V=V0)

    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)
    scheduler = Schedulers.partially(1/N)
    topology = complete_graph(N)

    society = odsociety(; agents, issues, topology, scheduler, rng, props...)
    
    return society
end

function spinglass_init(; agent_type, params...)
    spinglass_init(agent_type; params...)
end

function spinglass_naive_init(A::Type{<:AbstractEDNNAgent}; K=20, N=20, C0=10.0, V0=10.0, P=1, betaw0=1.0, betam0=1.0, seed=1234, props...)
    agents = A{K,N}|>
             rand.|>
             normalize!.|>
             x->scale_agent!(x; w=1.0, m=1.0, C=C0, V=V0)

    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)
    scheduler = Schedulers.partially(1/N)
    topology = complete_graph(N)

    society = odsociety(; agents, issues, topology, scheduler, rng, props...)
    
    return society
end

function spinglass_consensus_init(A::Type{<:AbstractEDNNAgent}; K=20, N=20, C0=10.0, V0=10.0, P=1, betaw0=1.0, betam0=1.0, seed=1234, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0)*betam0

    agents = [A{K,N}(
                    i,i,
                    normalize!(randn(K)).*w0,
                    ODEDNNA._C_identity(A{K,N}).*C0,
                    [i==j ? 0.0 : -1.0 for j in 1:N].*m0,
                    [i==j ? 0.0 : 1.0 for j in 1:N].*V0
                ) for i in 1:N]
              
    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)
    scheduler = Schedulers.partially(1/N)
    topology = complete_graph(N)

    society = odsociety(; agents, issues, topology, scheduler, rng, props...)
    
    return society
end

function spinglass_consensus_init(; agent_type, params...)
    spinglass_consensus_init(agent_type; params...)
end