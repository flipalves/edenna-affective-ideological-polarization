### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ a8bcb3ad-096d-4322-809c-3494eee3ffb1
using Pkg

# ╔═╡ 5c6451d0-b0dd-4999-aacf-022801054c55
using DrWatson

# ╔═╡ 3b56e49c-e0b3-4949-9af4-3ed51098c8e8
begin
    using Agents
    using Agents: AbstractSpace
    using BSON
    using DataFrames
    using Distances
    using Distributions
    using LinearAlgebra
    using MultivariateStats
    using ODEDNNA
    using Random
    using StatsBase
end

# ╔═╡ 9aa9d2a0-e51b-4b2b-bcb2-eb18f4eee038
begin
    include(srcdir("util.jl"))
    include(srcdir("data-collection.jl"))
end 

# ╔═╡ 95c11c11-0c7c-480d-a2e8-b3b1b009ccf1
@quickactivate "ednna-affective-ideological-polarization"

# ╔═╡ 2704b1fe-284d-407b-87b3-2824dd0df193
function scenario_spin_glass_init(; K, N, C0, V0, P, betaw0, betam0, seed, props...)
    w0 = sqrt(1 + C0)*betaw0
    m0 = sqrt(1 + V0)*betam0

    agents = ODAgentIHC{K,N}|>
             rand.|>
             normalize!.|>
             x->scale_agent!(x; w=w0, m=m0, C=C0, V=V0)

    issues = random_issues(;K, P)
    rng = Random.MersenneTwister(seed)

    return odsociety(; agents, issues, rng, props...)
end

# ╔═╡ 326c2200-8a16-4126-a9e4-20a75f0a245b
scenario_spin_glass_init(; K=5, N=30, C0=1.0, V0=1.0, P=1, betaw0=1.0, betam0=1.0, seed=1234)

# ╔═╡ 25877bd4-70e9-4025-a1bf-41f0fd412f08
spinglass_simulator = SimulationSetup(
                            model_init  = scenario_spin_glass_init,
                            agent_step! = entropic_dynamics!,
							mdata       = [mean_balance],
            				when_model  = true,
                            parallel    = false,
                            prefix      = "spinglass-thesis",
                            suffix      = "bson",
                            path        = datadir("sims","N_greater_than_2")
						)

# ╔═╡ 0b37316b-ac01-45d8-ae2d-fd0376c283f1
function spinglass_params(; K=20, N=20, C0=10.0, V0=10.0, betaw0=1.0, betam0=1.0, P=[1,2,4,8,16,20,24,48,100,400,8_000,160_000],replicates=1)  
	# following my thesis 
    seed = [rand(UInt8) for _ in 1:replicates]
    parameters = @dict(K,N,C0,V0,betaw0,betam0,P,seed)
    return parameters
end

# ╔═╡ 2867d28e-115e-472d-88bd-de1bab8e36b4
spinglass_params()

# ╔═╡ 40170752-ce39-4e58-b4b9-83ad3e4ad3dd
Pkg.status()

# ╔═╡ 251b053d-6a13-4331-a9ce-ad37a08ead13
spinglass_simulator[steps=1,prefix="spinglass-test",parallel=false](; force=true, spinglass_params()...)

# ╔═╡ d864ecb8-b429-11eb-3e31-714e766d4b09
##
# using DrWatson
# @quickactivate "ednna-affective-ideological-polarization"

# NCORES = length(Sys.cpu_info())
# using Distributed
# nprocs()<NCORES && addprocs(NCORES)

# @everywhere using DrWatson
# @everywhere @quickactivate "ednna-affective-ideological-polarization"

# @everywhere begin
#     using Agents
#     using Agents: AbstractSpace
#     using BSON
#     # using Chain
#     using DataFrames
#     using Distances
#     using Distributions
#     using LinearAlgebra
#     using MultivariateStats
#     using ODEDNNA
#     using Random
#     # using StaticArrays
#     using StatsBase
# end

# using CSVFiles
# using IntervalArithmetic
# using Queryverse
# using UnicodeFun

# using RCall
# @rlibrary ggplot2
# @rlibrary patchwork
# @rlibrary ggthemes
# @rlibrary ggpointdensity
# @rlibrary ggridges
# @rlibrary latex2exp
# @rlibrary scales

# @everywhere begin
#     include(srcdir("util.jl"))
#     include(srcdir("data-collection.jl"))
# end 

##
# @everywhere function scenario_spin_glass(; K, N, C0, V0, P, betaw0, betam0, seed, props...)
#     w0 = sqrt(1 + C0)*betaw0
#     m0 = sqrt(1 + V0)*betam0

#     agents = ODAgentIHC{K,N}|>
#              rand.|>
#              normalize!.|>
#              x->scale_agent!(x; w=w0, m=m0, C=C0, V=V0)

#     issues = random_issues(;K, P)
#     rng = Random.MersenneTwister(seed)

#     return odsociety(; agents, issues, rng, props...)
# end

##
# function run_sim(;sweeps=1,replicates=1, force=false)
#     parameters=(K=20,N=20,C0=10.0,V0=10.0,betaw0=1.0,betam0=1.0)
#     #K = parameters.K
#     #P = [1:K...,2K,5K,10K,20K,50K,100K]
#     # following my thesis 
#     P = [1,2,4,8,16,20,24,48,100,400,8_000]#,160_000]
#     seed = [rand(UInt8) for _ in 1:replicates]
#     parameters = (;parameters...,P,seed)
#     d = complexity(parameters.K,parameters.N)
#     total_steps = sweeps*d
#     # report_instants(ts) = (m,s)->s in ts
#     # instants_to_report = (Int∘round).(StatsBase.quantile(0:total_steps,[0.0,0.25,0.5,0.75,1.0]))

#     spinglass_simulator = SimulationSetup(
#                             model_init=scenario_spin_glass,
#                             agent_step! = entropic_dynamics!,
#                             # adata=[:w,:m,m_sqrt1pV],
#                             # when=periodic_report(10),
#                             mdata=[mean_balance],
#                             when_model=periodic_report(parameters.N),#report_instants(instants_to_report),#periodic_report(10),
#                             steps=total_steps,
#                             parallel=true,
#                             prefix="spinglass-thesis",
#                             suffix="bson",
#                             path=datadir("sims","N_greater_than_2")
#                         )

#     datadict, fname = spinglass_simulator(; force, parameters...)
    
#     data = datadict[:mdata]
#     summary_data = data |>
#         df->groupby(df,[:step,:P])|>
#         df->combine(df,:mean_balance=>mean=>:avg_mean_balance)|>
#         df->transform!(df,:P=>ByRow(x->"P == $x")=>:number_of_issues,:step=>ByRow(x->log10(1+x))=>:log10_t)|>
#         DataFrame

#     return summary_data, parameters
# end
    
# function plot_sim(data, parameters)
#     spinglass_plot = ggplot(data, aes(x=:log10_t,y=:avg_mean_balance,color=:number_of_issues)) +
#         geom_point(size=1,shape=21,alpha=7/10) + geom_line()+
#         scale_color_viridis_d()

#     ggsave(savename("spinglass-balance",ntuple2dict(parameters),"pdf"),
#             spinglass_plot,
#             path=plotsdir("simulations","N_greater_than_2"),
#             height=6,width=10)

#     return spinglass_plot
# end
##


# ╔═╡ Cell order:
# ╠═5c6451d0-b0dd-4999-aacf-022801054c55
# ╠═95c11c11-0c7c-480d-a2e8-b3b1b009ccf1
# ╠═3b56e49c-e0b3-4949-9af4-3ed51098c8e8
# ╠═9aa9d2a0-e51b-4b2b-bcb2-eb18f4eee038
# ╠═2704b1fe-284d-407b-87b3-2824dd0df193
# ╠═326c2200-8a16-4126-a9e4-20a75f0a245b
# ╠═25877bd4-70e9-4025-a1bf-41f0fd412f08
# ╠═0b37316b-ac01-45d8-ae2d-fd0376c283f1
# ╠═2867d28e-115e-472d-88bd-de1bab8e36b4
# ╠═a8bcb3ad-096d-4322-809c-3494eee3ffb1
# ╠═40170752-ce39-4e58-b4b9-83ad3e4ad3dd
# ╠═251b053d-6a13-4331-a9ce-ad37a08ead13
# ╠═d864ecb8-b429-11eb-3e31-714e766d4b09
